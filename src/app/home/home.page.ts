import { Component } from '@angular/core';
import { MSAdal, AuthenticationContext, AuthenticationResult } from '@ionic-native/ms-adal/ngx';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  private authContext: AuthenticationContext;

  constructor(private msAdal: MSAdal) {}


  test2(){
    let authContext: AuthenticationContext = this.msAdal.createAuthenticationContext('https://login.windows.net/common');

authContext.acquireTokenAsync('https://graph.windows.net', 'a5d92493-ae5a-4a9f-bcbf-9f1d354067d3', 'http://MyDirectorySearcherApp')
  .then((authResponse: AuthenticationResult) => {
    console.log('Token is' , authResponse.accessToken);
    console.log('Token will expire on', authResponse.expiresOn);
  })
  .catch((e: any) => console.log('Authentication failed', e));

  }

  async test(): Promise<AuthenticationResult | null> {
    let authContext: AuthenticationContext = this.msAdal.createAuthenticationContext('https://login.windows.net/common');
    const authResponse: AuthenticationResult = await authContext.acquireTokenAsync(
      'https://graph.microsoft.com',
      '7c0b1b7d-25c7-424e-9a5a-6c88eae4bdd7',
      'http://localhost:8100/home',
      '',
      ''
    );

      return authResponse;
  }
}
